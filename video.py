import ffmpeg

def video_meta_data(file):
    probe = ffmpeg.probe(file)
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    if video_stream is None:
        print("No video stream found")
        return
    width = int(video_stream['width'])
    height = int(video_stream['height'])
    codec = video_stream['codec_name']
    bit_rate = int(video_stream['bit_rate'])
    # frame_rate = float(video_stream['avg_frame_rate'])
    print(f"Video resolution: {width}x{height}")
    print(f"Video codec: {codec}")
    print(f"Bit rate: {bit_rate}")
    # print(f"Frame rate: {frame_rate}")
    # print(video_stream.keys())






video_meta_data("dataset/video_drs.mp4")

