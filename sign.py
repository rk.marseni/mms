import cv2
import subprocess

# Read the video
video = cv2.VideoCapture("dataset/video_drs.mp4")

# Get video dimensions
width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

# Define the codec and create a video writer object
fourcc = cv2.VideoWriter_fourcc(*"mp4v")
out = cv2.VideoWriter("dataset/video_drss.mp4", fourcc, 30.0, (width, height))

# Define the text and font
text = "Congratulation Team DRS !"
font = cv2.FONT_HERSHEY_SIMPLEX
color = (255, 255, 255)
thickness = 2

# Add watermark to each frame
while True:
    ret, frame = video.read()
    if not ret:
        break
    # cv2.putText(frame, text, (10, height - 10), font, 1, color, thickness, cv2.LINE_AA)
    out.write(frame)

# Release resources
video.release()
out.release()

# Use FFmpeg to add the watermark to the video
subprocess.call(["ffmpeg", "-i", 
                    "dataset/video_drs.mp4", 
                    "-vf", 
                    "drawtext=text='{}':fontsize=24:fontcolor=white:x=(w-text_w)/2:y=(h-text_h)".format(text), 
                    "dataset/watermarked_output.mp4"])
