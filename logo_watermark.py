import cv2
import subprocess

# Load the watermark image
watermark = cv2.imread("logo.jpg", cv2.IMREAD_UNCHANGED)

# Get the video capture object
cap = cv2.VideoCapture("video_drs.mp4")

# Get the codec information from the input video
fourcc = int(cap.get(cv2.CAP_PROP_FOURCC))
fps = cap.get(cv2.CAP_PROP_FPS)
frame_size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
watermark=cv2.resize(watermark, (frame_size[0], frame_size[1]))
# Define the codec and create a video writer object
out = cv2.VideoWriter("output.mp4", fourcc, fps, frame_size, isColor=True)

# Iterate over each frame of the video
while True:
    # Read the next frame
    ret, frame = cap.read()

    # If we've reached the end of the video, break out of the loop
    if not ret:
        break

    # Add the watermark to the frame
    frame = cv2.addWeighted(frame, 1, watermark, 0.3, 0)

    # Write the frame to the output video
    out.write(frame)

# Release the video capture and writer objects
cap.release()
out.release()

# Use FFmpeg to overlay the watermark image onto the video
subprocess.call(["ffmpeg", "-i", "video_drs.mp4", "-i", "logo.jpg", "-filter_complex", "overlay=10:10", "output.mp4"])
